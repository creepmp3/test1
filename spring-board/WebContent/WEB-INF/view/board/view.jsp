<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
table {
    width:600px;
}
table, th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    width:20%;
    background-color: #dedede;
}
table tr:nth-child(3) {
    height:200px;
}

div {
    border: 0px solid black;
    margin-top:10px;
    width:600px;
    text-align:right;
}


</style>
</head>
<body>
    <form action="boardInsertOk" method="post">
        <table>
            <tr>
                <th>작성자</th>
                <td>${dto.writer }</td>
            </tr>
            <tr>
                <th>제목</th>
                <td>${dto.title }</td>
            </tr>
            <tr>
                <th>내용</th>
                <td>${dto.contents }</td>
            </tr>
            <tr>
                <th>IP</th>
                <td>${dto.ip }</td>
            </tr>
            <tr>
                <th>조회수</th>
                <td>${dto.readcount }</td>
            </tr>
            <tr>
                <th>추천수</th>
                <td>${dto.hits }</td>
            </tr>
            <tr>
                <th>등록일</th>
                <td>${dto.regdate }</td>
            </tr>
        </table>
    </form>
    
    
    <div>
        <a href="<c:url value='/board/hits?bno=${dto.bno }'></c:url>"><input type="button" value="추천"/></a>
        <a href="<c:url value='/board/modify'></c:url>"><input type="button" value="수정"/></a>
        <a href="<c:url value='/board/list'></c:url>"><input type="button" value="목록"/></a>
    </div>
    
</body>
</html>